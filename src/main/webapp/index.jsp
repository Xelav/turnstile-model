<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@page import="model.Model"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type">
    <title> Турникет </title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWc+VRZkx5xU3daN+lYb49+Ax+Tl" crossorigin="anonymous"></script>
</head>
<body>

<div class="container">
	<!-- Заголовок страницы -->
    <h1 class="mt-4">Модель турникета</h1>
    <hr/>
	
	<!-- Форма ввода параметров -->
    <h2 class="mb-4">Введите параметры моделирования</h2>
    <form action="/model" method="post" class="form">
        <div class="form-group row">
            <label class="col-sm-4 col-form label">Интенсивность прихода новых пассажиров</label>
            <div class="col-sm-2">
                <input class="form-control" placeholder="" name="incomingIntensity" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 col-form label">Интенсивность обслуживания</label>
            <div class="col-sm-2">
                <input class="form-control" placeholder="" name="servingIntensity" />
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 col-form label">Максимальный размер очереди</label>
            <div class="col-sm-2">
                <input class="form-control" placeholder="" name="maxQueueSize" />
            </div>
        </div>
        <input type="submit" value="Старт" class="btn btn-primary" />
    </form>
    <hr />
</div>

<div class="container">
    <%
		// берём атрибут, содержащий модель
        Model model =(Model) request.getAttribute("model");
		// если модель не пустая, выводим её состояние
        if (model != null) {
    %>
	<!-- Форма вывода состояния модели -->
    <div class="card mt-2 border-dark col-sm-6">
        <div class="card-header bg-transparent border-dark">
            <h2 class="">Статистика</h2>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-6 col-form label">Такт</label>
                <div class="col-sm-2">
                    <input readonly class="form-control-plaintext" value="<%= model.getCurrentTick()%>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-6 col-form label">Размер очереди</label>
                <div class="col-sm-2">
                    <input readonly class="form-control-plaintext" value="<%= model.getPassengerQueueSize()%>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-6 col-form label">Пассажиров пропущено</label>
                <div class="col-sm-2">
                    <input readonly class="form-control-plaintext" value="<%= model.getPassengersPassed()%>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-6 col-form label">Пассажиров не прошло</label>
                <div class="col-sm-2">
                    <input readonly class="form-control-plaintext" value="<%= model.getPassengersRejected()%>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-6 col-form label">Работает</label>
                <div class="col-sm-2">
                    <input readonly class="form-control-plaintext" value="<%=!model.isStopped() ? "Да" : "Нет"%>" />
                </div>
            </div>
        </div>
    </div>
    <form action="/model" method="get">
        <input type='submit' value='Обновить состояние моделирования' class="btn btn-primary mt-2"/>
    </form>
    <%
    } else { // Если модели нет, то отрисовываем сообщение
    %>
    <h2>Модель не инициализирована</h2>
    <%
        }
    %>
</div>
</body>
</html>