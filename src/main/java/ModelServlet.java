import model.Model;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
        name = "modelServlet",
        urlPatterns = "/model"
)
public class ModelServlet extends HttpServlet {

    private static Model model;

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response
    ) throws ServletException, IOException {
        request.setAttribute("model", this.model);
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
                           HttpServletResponse response
    ) throws ServletException, IOException {

        double incomingIntensity;
        double servingIntensity;
        int maxQueueSize;

        try {
            incomingIntensity = Double.valueOf(request.getParameter("incomingIntensity"));
            servingIntensity = Double.valueOf(request.getParameter("servingIntensity"));
            maxQueueSize = Integer.valueOf(request.getParameter("maxQueueSize"));
        } catch (NumberFormatException ex) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        this.model = new Model(incomingIntensity ,servingIntensity ,maxQueueSize);
        Thread modelThread = new Thread(model);
        modelThread.start();

        request.setAttribute("model", this.model);
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

    public static Model getModel() {
        return model;
    }
}