package model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Random;

class Passenger {

    private LocalDate ticketExpiration;

    Passenger() {
        LocalDate d1 = LocalDate.now().plusDays(10);
        LocalDate d2 = LocalDate.now().minusDays(10);

        long days = ChronoUnit.DAYS.between(d2, d1);
        LocalDate randomDate = d1.plusDays(new Random().nextInt((int) days + 1));

        ticketExpiration=randomDate;
    }

    LocalDate getTicketExpiration() {
        return ticketExpiration;
    }
}
