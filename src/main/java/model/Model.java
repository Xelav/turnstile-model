package model;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Queue;

public class Model  implements Runnable {

    private double incomingIntensity;
    private double servingIntensity;
    private int maxQueueSize;
    private final double rejectionProbability = 0.3;
    private int passengersPassed;
    private int passengersRejected;

    private boolean started;
    private boolean stopped;

    private int currentTick;

    Queue<Passenger> passengerQueue;

    public Model(
            double incomingIntensity,
            double servingIntensity,
            int maxQueueSize) {

        assert incomingIntensity > 0;
        assert servingIntensity > 0;
        assert  maxQueueSize > 0;

        this.incomingIntensity = incomingIntensity;
        this.servingIntensity = servingIntensity;
        this.maxQueueSize = maxQueueSize;
        passengerQueue = new LinkedList<Passenger>();

        passengersPassed = 0;
        passengersRejected = 0;

        currentTick = 0;

        started = false;
        stopped = false;
    }

    private void tick() {
        currentTick++;
        int incomingNumber = generatePoisson(incomingIntensity);
        int servingNumber = generatePoisson(servingIntensity);

        for(int i=0; i < incomingNumber; i++){
            Passenger passenger = new Passenger();
            passengerQueue.add(passenger);
        }

        for (int i=0; i < servingNumber; i++) {
            Passenger passenger = passengerQueue.poll();
            if (passenger == null) {
                break;
            }
            this.servePassenger(passenger);
        }

        if (passengerQueue.size() > maxQueueSize) {
            this.stop();
        }
    }

    public void printState() {
        System.out.println(String.format("\n=====%d======", currentTick));
        System.out.println(String.format("Queue size - %d", this.passengerQueue.size()));
        System.out.println(String.format("Passed     - %d", this.passengersPassed));
        System.out.println(String.format("Rejected   - %d", this.passengersRejected));
    }

    public void run() {
        if (started) {
            throw new RuntimeException("Model already started");
        } else {
            started = true;
            while (!this.stopped) {
                this.tick();
                printState();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stop() {
        this.stopped = true;
    }

    private void servePassenger(Passenger passenger) {
        LocalDate currentDate = LocalDate.now();
        boolean rejected = Math.random() < this.rejectionProbability;
        boolean expired = currentDate.isAfter(passenger.getTicketExpiration());
        if (rejected || expired) {
            this.passengersRejected++;
        } else {
            this.passengersPassed++;
        }

    }

    public static int generatePoisson(double lambda) {
        // https://en.wikipedia.org/wiki/Poisson_distribution#Generating_Poisson-distributed_random_variables
        assert lambda > 0;
        double L = Math.exp(-lambda);
        double p = 1.0;
        int k = 0;

        do {
            k++;
            p *= Math.random();
        } while (p > L);

        return k - 1;
    }

    public int getPassengerQueueSize(){
        return  this.passengerQueue.size();
    }

    public int getPassengersPassed(){
        return this.passengersPassed;
    }

    public int getPassengersRejected(){
        return this.passengersRejected;
    }

    public int getCurrentTick(){
        return this.currentTick;
    }

    public boolean isStopped() {
        return stopped;
    }

    public boolean isStarted() {
        return started;
    }
}
