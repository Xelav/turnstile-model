import model.Model;
import static  org.junit.Assert.*;
import org.junit.Test;

import java.util.Random;


public class ModelTest {

    static Random random = new Random();

    private int nextPositiveNumber() {
        return random.nextInt(Integer.MAX_VALUE-1)+1;
    }

    @Test(expected = RuntimeException.class)
    public void testRepeatedStart() throws InterruptedException {
        Model model = new Model(1,1,100);

        Thread modelThread = new Thread(model);
        modelThread.start();

        Thread.sleep(50);
        assertTrue(model.isStarted());
        modelThread.start();

    }

    @Test(expected = AssertionError.class)
    public void testNegativeIncomingIntensity() {
        Model model = new Model(-nextPositiveNumber(),nextPositiveNumber(),nextPositiveNumber());
    }

    @Test(expected = AssertionError.class)
    public void testNegativeServingIntensity() {
        Model model = new Model(nextPositiveNumber(),-nextPositiveNumber(),nextPositiveNumber());
    }

    @Test(expected = AssertionError.class)
    public void testNegativeMaxQueueSize() {
        Model model = new Model(nextPositiveNumber(),nextPositiveNumber(),-nextPositiveNumber());
    }

    @Test(expected = AssertionError.class)
    public void testZeroIncomingIntensity() {
        Model model = new Model(0,nextPositiveNumber(),nextPositiveNumber());
    }

    @Test(expected = AssertionError.class)
    public void testZeroServingIntensity() {
        Model model = new Model(nextPositiveNumber(),0,nextPositiveNumber());
    }

    @Test(expected = AssertionError.class)
    public void testZeroMaxQueueSize() {
        Model model = new Model(nextPositiveNumber(),nextPositiveNumber(),0);
    }


    @Test
    public void testManualStopping() throws InterruptedException {

        Model model = new Model(1,1,100);

        Thread modelThread = new Thread(model);
        modelThread.start();

        Thread.sleep(50);
        assertTrue(model.isStarted());
        model.stop();
        Thread.sleep(50);

        assertTrue(model.isStopped());

    }

    @Test
    public void testFullRun() throws InterruptedException {

        Model model = new Model(10,5,50);

        Thread modelThread = new Thread(model);
        modelThread.start();

        do {
            Thread.sleep(2000);
        } while (!model.isStopped());

        assertTrue(model.isStarted());
        assertTrue(model.isStopped());

        assertTrue(model.getCurrentTick() > 0);
        assertTrue(model.getPassengersPassed() > 0);
        assertTrue(model.getPassengersRejected() > 0);

    }

    @Test
    public void testPoissonWithPositiveLambda() {
        double lambda = random.nextDouble();
        int result = Model.generatePoisson(lambda);
        assertTrue(result >= 0);
    }

    @Test (expected = AssertionError.class)
    public void testPoissonWithNegativeLambda() {
        double lambda = -random.nextDouble();
        int result = Model.generatePoisson(lambda);
    }

    @Test (expected = AssertionError.class)
    public void testPoissonWithZeroLambda() {
        int result = Model.generatePoisson(0);
    }

}
