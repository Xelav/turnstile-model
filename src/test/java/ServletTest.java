import model.Model;
import org.junit.Before;
import org.junit.Test;


import javax.servlet.http.HttpServletResponse;

import static  org.junit.Assert.*;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.util.Random;


public class ServletTest {

    private ModelServlet servlet;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    private static Random random = new Random();
    private static final String NUMBERS_STRING = "0123456789";
    private static final String CHARACTERS_STRING = "qwertyuiopasdfghjklzxcvbnm";


    public static String generateNumericSequence(int size) {

        int count = size;
        StringBuilder builder = new StringBuilder();

        // чтобы не сгенерировать 0 в начале
        int character = random.nextInt(NUMBERS_STRING.length()-1)+1;
        builder.append(NUMBERS_STRING.charAt(character));

        while (count-- != 0) {
            character = random.nextInt(NUMBERS_STRING.length());
            builder.append(NUMBERS_STRING.charAt(character));
        }
        return builder.toString();
    }

    public static String generateDoubleNumericString() {
        // целочисленная часть
        int integerPartSize = random.nextInt(254)+1;
        String integerPart = generateNumericSequence(integerPartSize);
        // дробная часть
        int fractionalPartSize = random.nextInt(254)+1;
        String fractionalPart = generateNumericSequence(integerPartSize);

        return integerPart + "." + fractionalPart;
    }

    public static String generateIntegerString() {
        return Integer.toString(Math.abs(random.nextInt())+1);
    }

    public static String generateRandomWord() {

        int count = random.nextInt(254)+1;
        StringBuilder builder = new StringBuilder();

        while (count-- != 0) {
            int character = random.nextInt(CHARACTERS_STRING.length());
            builder.append(CHARACTERS_STRING.charAt(character));
        }
        return builder.toString();
    }

    @Before
    public void setUp() {
        servlet = new ModelServlet();
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
    }

    @Test
    public void testGet() throws Exception {

        servlet.doGet(request, response);
        assertEquals("/index.jsp", response.getForwardedUrl());

        Model model = servlet.getModel();
        assertNull(model);

    }

    @Test
    public void testPost() throws Exception {

        request.addParameter("incomingIntensity", generateDoubleNumericString());
        request.addParameter("servingIntensity", generateDoubleNumericString());
        request.addParameter("maxQueueSize", generateIntegerString());
        servlet.doPost(request, response);

        assertEquals("/index.jsp", response.getForwardedUrl());

        Model model = servlet.getModel();
        assertNotNull(model);
        Thread.sleep(50);
        assertTrue(model.isStarted());

    }

    @Test
    public void testWrongIncomingIntensity() throws Exception {

        request.addParameter("incomingIntensity", generateRandomWord());
        request.addParameter("servingIntensity", generateDoubleNumericString());
        request.addParameter("maxQueueSize", generateIntegerString());
        servlet.doPost(request, response);

        assertEquals(HttpServletResponse.SC_BAD_REQUEST, response.getStatus());

    }

    @Test
    public void testWrongServingIntensity() throws Exception {

        request.addParameter("incomingIntensity", generateDoubleNumericString());
        request.addParameter("servingIntensity", generateRandomWord());
        request.addParameter("maxQueueSize", generateIntegerString());
        servlet.doPost(request, response);

        assertEquals(HttpServletResponse.SC_BAD_REQUEST, response.getStatus());

    }

    @Test
    public void testWrongMaxQueueSize() throws Exception {

        request.addParameter("incomingIntensity", generateDoubleNumericString());
        request.addParameter("servingIntensity", generateDoubleNumericString());
        request.addParameter("maxQueueSize", generateRandomWord());
        servlet.doPost(request, response);

        assertEquals(HttpServletResponse.SC_BAD_REQUEST, response.getStatus());

    }

    @Test
    public void testLargeMaxQueueSize() throws Exception {

        // сгенерируем большую строку с числом, превыщающее число 2,147,483,647
        int count = random.nextInt(244)+11;
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*NUMBERS_STRING.length());
            builder.append(NUMBERS_STRING.charAt(character));
        }
        String maxQueueSizeParam = builder.toString();

        request.addParameter("incomingIntensity", generateDoubleNumericString());
        request.addParameter("servingIntensity", generateDoubleNumericString());
        request.addParameter("maxQueueSize", maxQueueSizeParam);
        servlet.doPost(request, response);

        assertEquals(HttpServletResponse.SC_BAD_REQUEST, response.getStatus());

    }


}
